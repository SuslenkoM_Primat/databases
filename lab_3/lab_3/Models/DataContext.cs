﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace lab_3
{
    public class DataContext : DbContext
    {
        public DbSet<Employee> employee { get; set; }
        public DbSet<Profession> profession { get; set; }
        public DbSet<Station> railway_station { get; set; }
        public DbSet<Route> railway_route { get; set; }
        public DbSet<Relation> r1 { get; set; }

        public DataContext()
        {
            Database.EnsureCreated();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Database=lab1;Username=postgres;Password=pswd");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Employee>()
                .HasOne(p => p.employee_profession)
                .WithMany(b => b.empl)
                .HasForeignKey(p => p.employee_profession_id);

            modelBuilder.Entity<Employee>()
                .HasOne(p => p.work_station)
                .WithMany(b => b.empl)
                .HasForeignKey(p => p.work_station_id);
        }
    }
}
