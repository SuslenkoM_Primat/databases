﻿using Npgsql;
using System;
using System.Data;
using System.Linq;
using System.Collections.Generic;

namespace lab_3
{
    public class Storage
    {
        private string wholeParam;

        public Storage(string _host, int _port, string _user, string _password, string _database)
        {
            wholeParam = "Server=" + _host + ";Port=" + _port.ToString() + ";User Id=" + _user + ";Password=" + _password + ";Database=" + _database + ";";
        }

        void Analyze(string query)
        {
            using (NpgsqlConnection con = GetConnection())
            {
                con.Open(); //opens the connection

                List<string> analyzeList = new List<string>();

                using (NpgsqlCommand command = new NpgsqlCommand("EXPLAIN ANALYSE " + query, con))
                {
                    NpgsqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        analyzeList.Add(reader[0].ToString());
                    }
                }

                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine(analyzeList[analyzeList.Count - 2]);
                Console.WriteLine(analyzeList[analyzeList.Count - 1]);
                Console.ForegroundColor = ConsoleColor.White;
            }
        }

        public bool InputRoute(string name, int length)
        {
            try {
                using (DataContext db = new DataContext())
                {
                    Route temp = new Route();
                    temp.route_id = 4;
                    temp.name = name;
                    temp.length = length;


                    db.railway_route.Add(temp);
                    db.SaveChanges();
                    return true;
                }
            }
            catch
            {
                return false;
            }

        }

        public bool UpdateRoute(int id, string name, int length)
        {
            try
            {
                using (DataContext db = new DataContext())
                {
                    var entity = db.railway_route.FirstOrDefault(item => item.route_id == id);

                    if (entity != null)
                    {
                        entity.name = name;
                        entity.length = length;

                        db.SaveChanges();

                        return true;
                    }
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

        public bool InputStation(string name, int trackNumber, int capacity)
        {
            try
            {
                using (DataContext db = new DataContext())
                {
                    Station temp = new Station();
                    temp.name = name;
                    temp.track_number = trackNumber;
                    temp.capacity = capacity;

                    db.railway_station.Add(temp);
                    db.SaveChanges();
                    return true;
                }
            }
            catch
            {
                return false;
            }    
        }

        public bool UpdateStation(int id, string name, int trackNumber, int capacity)
        {
            try
            {
                using (DataContext db = new DataContext())
                {
                    var entity = db.railway_station.FirstOrDefault(item => item.station_id == id);

                    if (entity != null)
                    {
                        entity.name = name;
                        entity.track_number = trackNumber;
                        entity.capacity = capacity;

                        db.SaveChanges();

                        return true;
                    }
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

        public bool InputProfession(string name, string description)
        {
            try
            {
                using (DataContext db = new DataContext())
                {
                    Profession temp = new Profession();
                    temp.name = name;
                    temp.description = description;

                    db.profession.Add(temp);
                    db.SaveChanges();
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        public bool UpdateProfession(int id, string name, string description)
        {
            try
            {
                using (DataContext db = new DataContext())
                {
                    var entity = db.profession.FirstOrDefault(item => item.profession_id == id);

                    if (entity != null)
                    {
                        entity.name = name;
                        entity.description = description;

                        db.SaveChanges();

                        return true;
                    }
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

        public bool InputEmployee(string name, string surname, int stationId, int professionId)
        {

            using (NpgsqlConnection con = GetConnection())
            {
                try
                {
                    using (DataContext db = new DataContext())
                    {
                        Employee temp = new Employee();
                        temp.name = name;
                        temp.surname = surname;
                        temp.work_station_id = stationId;
                        temp.employee_profession_id = professionId;

                        db.employee.Add(temp);
                        db.SaveChanges();
                        return true;
                    }
                }
                catch
                {
 
                    return false;
                }
            }
        }

        public bool UpdateEmployee(int id, string name, string surname, int stationId, int professionId)
        {
            try
            {
                using (DataContext db = new DataContext())
                {
                    var entity = db.employee.FirstOrDefault(item => item.employee_id == id);

                    if (entity != null)
                    {
                        entity.name = name;
                        entity.surname = surname;
                        entity.work_station_id = stationId;
                        entity.employee_profession_id = professionId;

                        db.SaveChanges();

                        return true;
                    }
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

        public List<Route> GetAllRoutes()
        {
            using (DataContext db = new DataContext())
            {
                var routes = new List<Route>();

                foreach(Route temp in db.railway_route)
                {
                    routes.Add(temp);
                }

                return routes;
            }
        }

        public List<Employee> GetAllEmployees()
        {
            using(DataContext db = new DataContext())
            {
                var employees = new List<Employee>();

                foreach(Employee temp in db.employee)
                {
                    employees.Add(temp);
                }

                return employees;
            }
        }

        public List<Profession> GetAllProfessions()
        {
            using (DataContext db = new DataContext())
            {
                var professions = new List<Profession>();

                foreach (Profession temp in db.profession)
                {
                    professions.Add(temp);
                }

                return professions;
            }
        }

        public List<Station> GetAllStations()
        {
            using (DataContext db = new DataContext())
            {
                var stations = new List<Station>();

                foreach (Station temp in db.railway_station)
                {
                    stations.Add(temp);
                }

                return stations;
            }
        }

        public bool TestConnection()
        {
            using (NpgsqlConnection con = GetConnection())
            {
                con.Open();
                if (con.State == ConnectionState.Open)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public bool Connect(int routeId, int stationId)
        {
            try
            {
                using (DataContext db = new DataContext())
                {
                    Relation temp = new Relation();
                    temp.route_id = routeId;
                    temp.station_id = stationId;

                    db.r1.Add(temp);
                    db.SaveChanges();
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        public bool Disconnect(int routeId, int stationId)
        {
            try
            {
                using (DataContext db = new DataContext())
                {
                    var entity = db.r1.FirstOrDefault(item => item.route_id == routeId && item.station_id == stationId);

                    if (entity != null)
                    {
                        db.r1.Remove(entity);
                        db.SaveChanges();

                        return true;
                    }
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

        private NpgsqlConnection GetConnection()
        {
            return new NpgsqlConnection(wholeParam);
        }

        public bool Recruit(int stationId, int professionId, int number)
        {
            using (NpgsqlConnection con = GetConnection())
            {
                bool isOk = true;

                con.Open(); //opens the connection
                if (con.State == ConnectionState.Open)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Conneccted to a DB.");
                    Console.ForegroundColor = ConsoleColor.White;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Connecction failed.");
                    Console.ForegroundColor = ConsoleColor.White;
                    Environment.Exit(1);
                }

                try
                {
                    for (int i = 0; i < number; i++)
                    {
                        string query = "INSERT INTO public.employee (name, surname, work_station_id, employee_profession_id) " +
                                    "VALUES((select chr(trunc(65+random()*25)::int)";

                        Random rnd = new Random();
                        for(int j =0; j<rnd.Next(3,16); j++)
                        {
                            query += " || chr(trunc(97 + random() * 25)::int)";
                        }
                        query += "from generate_series(1, 1))" +

                        ", " +

                        "(select chr(trunc(65 + random() * 25)::int)";

                        for (int j = 0; j < rnd.Next(3, 16); j++)
                        {
                            query += " || chr(trunc(97 + random() * 25)::int)";
                        }

                        query += " from generate_series(1, 1)), " + stationId + ", " + professionId + "); ";

                        using (NpgsqlCommand command = new NpgsqlCommand(query, con))
                        {
                            command.ExecuteNonQuery();
                        }
                    }
                }
                catch
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Something went wrong.");
                    Console.ForegroundColor = ConsoleColor.White;
                    isOk = false;
                }
                return isOk;
            }
        }

        public bool RemoveEmployee(string name, string surname)
        {
            try
            {
                using (DataContext db = new DataContext())
                {
                    var entity = db.employee.FirstOrDefault(item => item.name == name && item.surname == surname);

                    if (entity != null)
                    {
                        db.employee.Remove(entity);
                        db.SaveChanges();

                        return true;
                    }
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

        public bool RemoveEmployee(int id)
        {
            try
            {
                try
                {
                    using (DataContext db = new DataContext())
                    {
                        var entity = db.employee.FirstOrDefault(item => item.employee_id == id);

                        if (entity != null)
                        {
                            db.employee.Remove(entity);
                            db.SaveChanges();

                            return true;
                        }
                        return false;
                    }
                }
                catch
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

        public bool RemoveProfession(string name)
        {
            try
            {
                using (DataContext db = new DataContext())
                {
                    var entity = db.profession.FirstOrDefault(item => item.name == name);

                    if (entity != null)
                    {
                        db.profession.Remove(entity);
                        db.SaveChanges();

                        return true;
                    }
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

        public bool RemoveProfession(int id)
        {
            try
            {
                using (DataContext db = new DataContext())
                {
                    var entity = db.profession.FirstOrDefault(item => item.profession_id == id);

                    if (entity != null)
                    {
                        db.profession.Remove(entity);
                        db.SaveChanges();

                        return true;
                    }
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

        public bool RemoveStation(string name)
        {
            try
            {
                using (DataContext db = new DataContext())
                {
                    var entity = db.railway_station.FirstOrDefault(item => item.name == name);

                    if (entity != null)
                    {
                        db.railway_station.Remove(entity);
                        db.SaveChanges();

                        return true;
                    }
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

        public bool RemoveStation(int id)
        {
            try
            {
                using (DataContext db = new DataContext())
                {
                    var entity = db.railway_station.FirstOrDefault(item => item.station_id == id);

                    if (entity != null)
                    {
                        db.railway_station.Remove(entity);
                        db.SaveChanges();

                        return true;
                    }
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

        public bool RemoveRoute(string name)
        {
            try
            {
                using (DataContext db = new DataContext())
                {
                    var entity = db.railway_route.FirstOrDefault(item => item.name == name);

                    if (entity != null)
                    {
                        db.railway_route.Remove(entity);
                        db.SaveChanges();

                        return true;
                    }
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

        public bool RemoveRoute(int id)
        {
            try
            {
                using (DataContext db = new DataContext())
                {
                    var entity = db.railway_route.FirstOrDefault(item => item.route_id == id);

                    if (entity != null)
                    {
                        db.railway_route.Remove(entity);
                        db.SaveChanges();

                        return true;
                    }
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

    }
}
