﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace lab_3
{
    public class Employee
    {
        [Key]
        public int employee_id { get; set; }
        public string name { get; set; }
        public string surname { get; set; }

        public int work_station_id { get; set; }
        public Station work_station { get; set; }

        public int employee_profession_id { get; set; }
        public Profession employee_profession { get; set; }
    }

    public class Profession
    {
        [Key]
        public int profession_id { get; set; }
        public string name { get; set; }
        public string description { get; set; }

        public List<Employee> empl { get; set; }
    }

    public class Station
    {
        [Key]
        public int station_id { get; set; }
        public string name { get; set; }
        public int track_number { get; set; }
        public int capacity { get; set; }

        public List<Employee> empl { get; set; }
    }

    public class Route
    {
        [Key]
        public int route_id { get; set; }
        public string name { get; set; }
        public int length { get; set; }
    }

    public class Relation
    {
        [Key]
        public int relation_id { get; set; }
        public int station_id { get; set; }
        public int route_id { get; set; }
    }
}
