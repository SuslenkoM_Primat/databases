﻿

# **Створення додатку бази даних, орієнтованого на взаємодію з СУБД PostgreSQL**

## employee
|name|data type|length/prec.|not null|PK|FK|
|--|--|--|--|--|--|
|employee_id|integer||yes|yes
|name|character varying|20|yes|no
| surname |character varying|20|yes|no
| work_station_id | integer||yes|no|railway_station.station_id
| employee_profession_id |integer||yes|no|profession.profession_id

## profession
|name|data type|length/prec.|not null|PK|FK|
|--|--|--|--|--|--|
|profession_id|integer||yes|yes
|name|character varying|20|yes|no
| description |character varying|256|no|no

## railway_route
|name|data type|length/prec.|not null|PK|FK|
|--|--|--|--|--|--|
|route_id|integer||yes|yes
|name|character varying|20|yes|no
| length |integer||yes|no

## railway_station
|name|data type|length/prec.|not null|PK|FK|
|--|--|--|--|--|--|
|station_id|integer||yes|yes
|name|character varying|20|yes|no
| track_number |integer||yes|no
| capacity |integer||yes|no

## R1
|name|data type|length/prec.|not null|PK|FK|
|--|--|--|--|--|--|
|relation_id|integer||yes|yes
|station_id|integer||yes|no|railway_station.station_id
| route_id |integer||yes|no|railway_route.route_id


