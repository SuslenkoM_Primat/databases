﻿using System;
using System.Collections.Generic;

namespace lab_2
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Interface.Run();
        }   
    }

    public static class Interface 
    {
        private static string input;
        private static string[] separated = new string[0];

        public static void Run()
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8; // для виводу кирилиці
            Console.InputEncoding = System.Text.Encoding.UTF8; // для вводу кирилиці
            Console.Clear();

            Storage local_storage = new Storage("localhost", 5432, "postgres", "pswd", "lab1");   

            while (true)
            {         
                if (separated.Length > 0)
                {
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.WriteLine("\nPrevious input:");
                    Console.ForegroundColor = ConsoleColor.White;

                    int i = 1;

                    foreach (string temp in separated)
                    {
                        Console.WriteLine("   "+ i++ +") "+temp);
                    }
                    Console.WriteLine();
                }

                MainMenuPrinter();

                input = Console.ReadLine();
                separated = input.Split('/');

                Console.Clear();

                if (separated.Length == 1)
                {
                    if (separated[0] == "exit")
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("\nProgram is stoped.\n");
                        Console.ForegroundColor = ConsoleColor.White;
                        break;
                    }
                    else if (separated[0] == "test")
                    {
                        Console.WriteLine("Testing connection...");

                        if (local_storage.TestConnection())
                        {
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("Conneccted to a DB.");
                            Console.ForegroundColor = ConsoleColor.White;
                        }
                        else
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("Connecction failed.");
                            Console.ForegroundColor = ConsoleColor.White;
                        }
                    }
                }
                else if (separated.Length == 2)
                {
                    if (separated[0] == "get")
                    {
                        if (separated[1] == "employee")
                        {
                            // Вывести всех сотрудников
                            EntityPrinter.Employees(local_storage.GetAllEmployeesWithStations());
                        }
                        else if (separated[1] == "profession")
                        {
                            // Вывести все профессии
                            EntityPrinter.Professions(local_storage.GetAllProfessions());
                        }
                        else if (separated[1] == "route")
                        {
                            // Вывести все профессии
                            EntityPrinter.Routes(local_storage.GetAllRoutesWithStations());
                        }
                    }
                }
                else if (separated.Length == 3)
                {
                    if (separated[0] == "connect")
                    {
                        if (!string.IsNullOrEmpty(separated[1]) &&  // route_id
                                !string.IsNullOrEmpty(separated[2]))    // station_id
                        {
                            if (int.TryParse(separated[1], out _) && int.TryParse(separated[2], out _))
                            {
                                if (int.Parse(separated[1]) > 0 && int.Parse(separated[2]) > 0)
                                {
                                    local_storage.Connect(int.Parse(separated[1]), int.Parse(separated[2]));
                                }
                                else
                                {
                                    Console.ForegroundColor = ConsoleColor.Red;
                                    Console.WriteLine("Params must be more than 0.");
                                    Console.ForegroundColor = ConsoleColor.White;
                                }
                            }
                            else
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Params must be integer.");
                                Console.ForegroundColor = ConsoleColor.White;
                            }
                        }
                        else
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("Params must be not empty.");
                            Console.ForegroundColor = ConsoleColor.White;
                        }
                    }
                    else if (separated[0] == "disconnect")
                    {
                        if (!string.IsNullOrEmpty(separated[1]) &&  // route_id
                                !string.IsNullOrEmpty(separated[2]))    // station_id
                        {
                            if (int.TryParse(separated[1], out _) && int.TryParse(separated[2], out _))
                            {
                                if (int.Parse(separated[1]) > 0 && int.Parse(separated[2]) > 0)
                                {
                                    local_storage.Disconnect(int.Parse(separated[1]), int.Parse(separated[2]));
                                }
                                else
                                {
                                    Console.ForegroundColor = ConsoleColor.Red;
                                    Console.WriteLine("Params must be more than 0.");
                                    Console.ForegroundColor = ConsoleColor.White;
                                }
                            }
                            else
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Params must be integer.");
                                Console.ForegroundColor = ConsoleColor.White;
                            }
                        }
                        else
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("Params must be not empty.");
                            Console.ForegroundColor = ConsoleColor.White;
                        }
                    }
                    else if (separated[0] == "remove")
                    {
                        if (separated[1] == "employee")
                        {
                            if (!string.IsNullOrEmpty(separated[2]))    // id
                            {
                                if (int.TryParse(separated[2], out _))
                                {
                                    if (int.Parse(separated[2]) > 0)
                                    {
                                        local_storage.RemoveEmployee(int.Parse(separated[2]));
                                    }
                                    else
                                    {
                                        Console.ForegroundColor = ConsoleColor.Red;
                                        Console.WriteLine("ID param must be more than 1.");
                                        Console.ForegroundColor = ConsoleColor.White;
                                    }
                                }
                                else
                                {
                                    Console.ForegroundColor = ConsoleColor.Red;
                                    Console.WriteLine("ID param must be integer.");
                                    Console.ForegroundColor = ConsoleColor.White;
                                }
                            }
                            else
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("ID param must be not empty.");
                                Console.ForegroundColor = ConsoleColor.White;
                            }
                        }
                        else if (separated[1] == "profession")
                        {
                            if (!string.IsNullOrEmpty(separated[2]))    // id OR name
                            {
                                if (int.TryParse(separated[2], out _))
                                {
                                    if (int.Parse(separated[2]) > 0)
                                    {
                                        local_storage.RemoveProfession(int.Parse(separated[2]));
                                    }
                                    else
                                    {
                                        Console.ForegroundColor = ConsoleColor.Red;
                                        Console.WriteLine("ID param must be more than 1.");
                                        Console.ForegroundColor = ConsoleColor.White;
                                    }
                                }
                                else
                                {
                                    local_storage.RemoveProfession(separated[2]);
                                }
                            }
                            else
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Param must be not empty.");
                                Console.ForegroundColor = ConsoleColor.White;
                            }
                        }
                        else if (separated[1] == "station")
                        {
                            if (!string.IsNullOrEmpty(separated[2]))    // id OR name
                            {
                                if (int.TryParse(separated[2], out _))
                                {
                                    if (int.Parse(separated[2]) > 0)
                                    {
                                        local_storage.RemoveStation(int.Parse(separated[2]));
                                    }
                                    else
                                    {
                                        Console.ForegroundColor = ConsoleColor.Red;
                                        Console.WriteLine("ID param must be more than 1.");
                                        Console.ForegroundColor = ConsoleColor.White;
                                    }
                                }
                                else
                                {
                                    local_storage.RemoveStation(separated[2]);
                                }
                            }
                            else
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Param must be not empty.");
                                Console.ForegroundColor = ConsoleColor.White;
                            }
                        }
                        else if (separated[1] == "route")
                        {
                            if (!string.IsNullOrEmpty(separated[2]))    // id OR name
                            {
                                if (int.TryParse(separated[2], out _))
                                {
                                    if (int.Parse(separated[2]) > 0)
                                    {
                                        local_storage.RemoveRoute(int.Parse(separated[2]));
                                    }
                                    else
                                    {
                                        Console.ForegroundColor = ConsoleColor.Red;
                                        Console.WriteLine("ID param must be more than 1.");
                                        Console.ForegroundColor = ConsoleColor.White;
                                    }
                                }
                                else
                                {
                                    local_storage.RemoveRoute(separated[2]);
                                }
                            }
                            else
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Param must be not empty.");
                                Console.ForegroundColor = ConsoleColor.White;
                            }
                        }
                    }
                }
                else if (separated.Length == 4)
                {
                    if (separated[0] == "put")
                    {
                        if (separated[1] == "route")
                        {
                            Console.WriteLine("Trying to input a data about new route...");

                            if (!string.IsNullOrEmpty(separated[2]) &&  // name
                                !string.IsNullOrEmpty(separated[3]))    // length
                            {
                                if (int.TryParse(separated[3], out _))
                                {
                                    if (int.Parse(separated[3]) > 0)
                                    {
                                        local_storage.InputRoute(separated[2], int.Parse(separated[3]));
                                    }
                                    else
                                    {
                                        Console.ForegroundColor = ConsoleColor.Red;
                                        Console.WriteLine("Route length must be more than 0.");
                                        Console.ForegroundColor = ConsoleColor.White;
                                    }
                                }
                                else
                                {
                                    Console.ForegroundColor = ConsoleColor.Red;
                                    Console.WriteLine("Route length must be integer.");
                                    Console.ForegroundColor = ConsoleColor.White;
                                }
                            }
                            else
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Params must be not empty.");
                                Console.ForegroundColor = ConsoleColor.White;
                            }
                        }
                        else if (separated[1] == "profession")
                        {
                            Console.WriteLine("Trying to input a data about new profession...");

                            if (!string.IsNullOrEmpty(separated[2]))   // name
                            {
                                local_storage.InputProfession(separated[2], separated[3]);
                            }
                            else
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Name param must be not empty.");
                                Console.ForegroundColor = ConsoleColor.White;
                            }
                        }

                    }
                    else if (separated[0] == "remove")
                    {
                        if (separated[1] == "employee")
                        {
                            if (!string.IsNullOrEmpty(separated[2]) &&  // name
                                !string.IsNullOrEmpty(separated[3]))    // surname
                            {
                                local_storage.RemoveEmployee(separated[2], separated[3]);   
                            }
                            else
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Params must be not empty.");
                                Console.ForegroundColor = ConsoleColor.White;
                            }
                        }
                    }
                    else if (separated[0] == "recruit")
                    {
                        if (!string.IsNullOrEmpty(separated[1]) &&    // station id
                             !string.IsNullOrEmpty(separated[2]) &&   // profession id
                             !string.IsNullOrEmpty(separated[3]))     // number of employees
                        {
                            if (int.TryParse(separated[1], out _) && int.TryParse(separated[2], out _) && int.TryParse(separated[3], out _))
                            {
                                if (int.Parse(separated[1]) > 0 && int.Parse(separated[2]) > 0 && int.Parse(separated[3]) > 0)
                                {
                                    local_storage.Recruit(int.Parse(separated[1]), int.Parse(separated[2]), int.Parse(separated[3]));
                                }
                                else
                                {
                                    Console.ForegroundColor = ConsoleColor.Red;
                                    Console.WriteLine("Params must be more than 1.");
                                    Console.ForegroundColor = ConsoleColor.White;
                                }
                            }
                            else
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Params must be integer.");
                                Console.ForegroundColor = ConsoleColor.White;
                            }
                        }
                    }
                }
                else if (separated.Length == 5)
                {
                    if (separated[0] == "put")
                    {
                        if (separated[1] == "station")
                        {
                            Console.WriteLine("Trying to input a data about new station...");

                            if (!string.IsNullOrEmpty(separated[2]) &&  // name
                                !string.IsNullOrEmpty(separated[3]) &&  // track number
                                !string.IsNullOrEmpty(separated[4]))    // capacity
                            {
                                if (int.TryParse(separated[3], out _) && int.TryParse(separated[4], out _))
                                {
                                    if (int.Parse(separated[3]) > 1 && int.Parse(separated[3]) > 1)
                                    {
                                        local_storage.InputStation(separated[2], int.Parse(separated[3]), int.Parse(separated[4]));
                                    }
                                    else
                                    {
                                        Console.ForegroundColor = ConsoleColor.Red;
                                        Console.WriteLine("Track number and capacity must be more than 1.");
                                        Console.ForegroundColor = ConsoleColor.White;
                                    }
                                }
                                else
                                {
                                    Console.ForegroundColor = ConsoleColor.Red;
                                    Console.WriteLine("Track number and capacity must be integer.");
                                    Console.ForegroundColor = ConsoleColor.White;
                                }
                            }
                            else
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Params must be not empty.");
                                Console.ForegroundColor = ConsoleColor.White;
                            }
                        }
                    }
                    else if (separated[0] == "update")
                    {
                        if (separated[1] == "profession")
                        {
                            Console.WriteLine("Trying to update a data about the profession...");

                            if (!string.IsNullOrEmpty(separated[2]) && // id
                                !string.IsNullOrEmpty(separated[3]))   // name
                            {
                                if (int.TryParse(separated[2], out _))
                                {
                                    if (int.Parse(separated[2]) > 0)
                                    {
                                        local_storage.UpdateProfession(int.Parse(separated[2]), separated[3], separated[4]);
                                    }
                                    else
                                    {
                                        Console.ForegroundColor = ConsoleColor.Red;
                                        Console.WriteLine("Route length must be more than 0.");
                                        Console.ForegroundColor = ConsoleColor.White;
                                    }
                                }
                                else
                                {
                                    Console.ForegroundColor = ConsoleColor.Red;
                                    Console.WriteLine("ID must be an integer.");
                                    Console.ForegroundColor = ConsoleColor.White;
                                }
                            }
                            else
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("ID and name params must be not empty.");
                                Console.ForegroundColor = ConsoleColor.White;
                            }
                        }
                        else if (separated[1] == "route")
                        {
                            Console.WriteLine("Trying to input a data about new route...");

                            if (!string.IsNullOrEmpty(separated[2]) &&  // id
                                !string.IsNullOrEmpty(separated[3]) &&  // name
                                !string.IsNullOrEmpty(separated[4]))    // length
                            {
                                if (int.TryParse(separated[2], out _) && int.TryParse(separated[4], out _))
                                {
                                    if (int.Parse(separated[2]) > 0 && int.Parse(separated[4]) > 0)
                                    {
                                        local_storage.UpdateRoute(int.Parse(separated[2]), separated[3], int.Parse(separated[4]));
                                    }
                                    else
                                    {
                                        Console.ForegroundColor = ConsoleColor.Red;
                                        Console.WriteLine("Route length and ID must be more than 0.");
                                        Console.ForegroundColor = ConsoleColor.White;
                                    }
                                }
                                else
                                {
                                    Console.ForegroundColor = ConsoleColor.Red;
                                    Console.WriteLine("Route length and ID must be integer.");
                                    Console.ForegroundColor = ConsoleColor.White;
                                }
                            }
                            else
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Params must be not empty.");
                                Console.ForegroundColor = ConsoleColor.White;
                            }
                        }
                    }
                }
                else if (separated.Length == 6)
                {
                    if (separated[0] == "put")
                    {
                        if (separated[1] == "employee")
                        {
                            Console.WriteLine("Trying to input a data about new employee...");

                            if (!string.IsNullOrEmpty(separated[2]) &&   // name
                                !string.IsNullOrEmpty(separated[3]) &&   // surname
                                !string.IsNullOrEmpty(separated[4]) &&   // station id
                                !string.IsNullOrEmpty(separated[5]))     // profession id
                            {
                                if (int.TryParse(separated[4], out _) && int.TryParse(separated[5], out _))
                                {
                                    local_storage.InputEmployee(separated[2], separated[3], int.Parse(separated[4]), int.Parse(separated[4]));
                                }
                                else
                                {
                                    Console.ForegroundColor = ConsoleColor.Red;
                                    Console.WriteLine("IDs must be integer.");
                                    Console.ForegroundColor = ConsoleColor.White;
                                }
                            }
                        }
                        else
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("All params must be not empty.");
                            Console.ForegroundColor = ConsoleColor.White;
                        }
                    }
                    else if (separated[0] == "update")
                    {
                        if (separated[1] == "station")
                        {
                            Console.WriteLine("Trying to update a data about the station...");

                            if (!string.IsNullOrEmpty(separated[2]) &&  // id
                                !string.IsNullOrEmpty(separated[3]) &&  // name
                                !string.IsNullOrEmpty(separated[4]) &&  // track number
                                !string.IsNullOrEmpty(separated[5]))    // capacity
                            {
                                if (int.TryParse(separated[2], out _) && int.TryParse(separated[4], out _) && int.TryParse(separated[5], out _))
                                {
                                    if (int.Parse(separated[2]) > 1 && int.Parse(separated[4]) > 1 && int.Parse(separated[5]) > 1)
                                    {
                                        local_storage.UpdateStation(int.Parse(separated[2]), separated[3], int.Parse(separated[4]), int.Parse(separated[5]));
                                    }
                                    else
                                    {
                                        Console.ForegroundColor = ConsoleColor.Red;
                                        Console.WriteLine("Track number, id and capacity must be more than 1.");
                                        Console.ForegroundColor = ConsoleColor.White;
                                    }
                                }
                                else
                                {
                                    Console.ForegroundColor = ConsoleColor.Red;
                                    Console.WriteLine("Track number, id and capacity must be integer.");
                                    Console.ForegroundColor = ConsoleColor.White;
                                }
                            }
                            else
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Params must be not empty.");
                                Console.ForegroundColor = ConsoleColor.White;
                            }
                        }
                    }
                }
                else if (separated.Length == 7)
                {
                    if (separated[0] == "update")
                    {
                        if (separated[1] == "employee")
                        {
                            Console.WriteLine("Trying to update a data about the employee...");

                            if (!string.IsNullOrEmpty(separated[2]) &&   // id
                                !string.IsNullOrEmpty(separated[3]) &&   // name
                                !string.IsNullOrEmpty(separated[4]) &&   // surname
                                !string.IsNullOrEmpty(separated[5]) &&   // station id
                                !string.IsNullOrEmpty(separated[6]))     // profession id
                            {
                                if (int.TryParse(separated[2], out _) && int.TryParse(separated[5], out _) && int.TryParse(separated[6], out _))
                                {
                                    local_storage.UpdateEmployee(int.Parse(separated[2]), separated[3], separated[4], int.Parse(separated[5]), int.Parse(separated[6]));
                                }
                                else
                                {
                                    Console.ForegroundColor = ConsoleColor.Red;
                                    Console.WriteLine("IDs must be integer.");
                                    Console.ForegroundColor = ConsoleColor.White;
                                }
                            }
                        }
                        else
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("All params must be not empty.");
                            Console.ForegroundColor = ConsoleColor.White;
                        }
                    }
                }
            }
        }

        public static void MainMenuPrinter()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Main menu commands:");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(" ~ test - test connection");
            Console.WriteLine(" ~ exit - exit the program");

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("   Getting:");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("       - get/employee - all employees with station name");
            Console.WriteLine("       - get/profession - all professions");
            Console.WriteLine("       - get/route - routes with stations");

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("   Adding:");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("       - put/employee/[name]/[surname]/[station id]/[profession id] - add an employee");
            Console.WriteLine("       - put/profession/[name]/[description] - add a profession");
            Console.WriteLine("       - put/station/[name]/[track number]/[capacity] - add a station");
            Console.WriteLine("       - put/route/[name]/[length] - add a route");

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("   Updating:");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("       - update/employee/[id]/[name]/[surname]/[station id]/[profession id] - update an employee");
            Console.WriteLine("       - update/profession/[id]/[name]/[description] - update a profession");
            Console.WriteLine("       - update/station/[id]/[name]/[track number]/[capacity] - update a station");
            Console.WriteLine("       - update/route/[id]/[name]/[length] - update a route");

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("   Removing:");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("       - remove/employee/[name]/[surname] - remove an employee");
            Console.WriteLine("       - remove/employee/[id] - remove an employee by id");

            Console.WriteLine("       - remove/profession/[name] - remove a profession (with all members)");
            Console.WriteLine("       - remove/profession/[id] - remove a profession (with all members)");

            Console.WriteLine("       - remove/station/[name] - remove a station (with connections to routes)");
            Console.WriteLine("       - remove/station/[id] - remove a station (with connections to routes)");

            Console.WriteLine("       - remove/route/[name] - remove a route (but not the stations)");
            Console.WriteLine("       - remove/route/[id] - remove a route (but not the stations)");

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("   Other:");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("       - recruit/[station id]/[profession id]/[number of employees] - recruit employees");
            Console.WriteLine("       - connect/[route id]/[station id] - connect a station to a route");
            Console.WriteLine("       - disconnect/[route id]/[station id] - disconnect a station from a route");

            Console.WriteLine("");
        }
    }

    public static class EntityPrinter
    {
        public static void Employees(List<Employee> tempList)
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("\nAll railway employees:");
            Console.ForegroundColor = ConsoleColor.White;

            foreach (Employee solo in tempList)
            {
                Console.WriteLine("\n- - - - - - - - - - - - - - - - - - -");
                Console.WriteLine("-          ID: "+solo.id);
                Console.Write("-   ~~~    Name: ");
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.WriteLine(solo.name);
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write("-  {* *}   Surname: ");
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.WriteLine(solo.surname);
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("-   \\-/    Profession: " + solo.professionName);
                Console.WriteLine("-   | |    Station: " + solo.stationName);
                Console.WriteLine("- - - - - - - - - - - - - - - - - - -\n");
            }
        }

        public static void Professions(List<Profession> tempList)
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("\nProfessions:");
            Console.ForegroundColor = ConsoleColor.White;

            foreach (Profession solo in tempList)
            {
                Console.WriteLine("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");
                Console.WriteLine("-| ID: " + solo.id);
                Console.Write("-| Name: ");
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.WriteLine(solo.name);
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("-| Description: ");
                Console.WriteLine("-|    "+ solo.description);   
            }
            Console.WriteLine("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");
            Console.WriteLine();
        }

        public static void Routes(List<Route> tempList)
        {
            Console.WriteLine();
            foreach(Route soloRoute in tempList)
            {
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.WriteLine("Route "+soloRoute.name+", "+soloRoute.length+"km (ID "+soloRoute.id+")");
                Console.ForegroundColor = ConsoleColor.White;

                foreach (Station soloStation in soloRoute.stations)
                {
                    Console.WriteLine("   ~ "+soloStation.name+" (ID "+soloStation.id+", capacity: "+ soloStation.capacity+" people, track number: "+soloStation.trackNumber+")");
                }
            }
        }
    }
}




/*
bool otherOpts = false;

for (int i = 1; i < separated.Length; i++)
{
    if (!string.IsNullOrEmpty(separated[i]))
    {
        otherOpts = true;
    }
}

if (!otherOpts)
{
    // список всех сотрудников с именем станции
    Console.WriteLine("Here you`ll find all employees.\n");
}
*/