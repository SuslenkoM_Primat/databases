﻿using System;
using System.Collections.Generic;
using Npgsql;
using System.Data;

namespace lab_2
{
    public class Storage
    {
        private string host;
        private int port;
        private string user;
        private string password;
        private string database;

        private string wholeParam;

        public Storage(string _host, int _port, string _user, string _password, string _database)
        {
            this.host = _host;
            this.port = _port;
            this.user = _user;
            this.password = _password;
            this.database = _database;

            wholeParam = "Server=" + host + ";Port=" + port.ToString() + ";User Id=" + user + ";Password=" + password + ";Database="+database+";";
        }

        void Analyze(string query)
        {
            using (NpgsqlConnection con = GetConnection())
            {
                con.Open(); //opens the connection

                List<string> analyzeList = new List<string>();
                
                using (NpgsqlCommand command = new NpgsqlCommand("EXPLAIN ANALYSE " + query, con))
                {
                    NpgsqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        analyzeList.Add(reader[0].ToString());
                    }
                }

                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine(analyzeList[analyzeList.Count-2]);
                Console.WriteLine(analyzeList[analyzeList.Count - 1]);
                Console.ForegroundColor = ConsoleColor.White;

                con.Close();
            }
        }

        public bool InputRoute(string name, int length)
        {
            using (NpgsqlConnection con = GetConnection())
            {
                bool isOk = true;

                con.Open(); //opens the connection
                if (con.State == ConnectionState.Open)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Conneccted to a DB.");
                    Console.ForegroundColor = ConsoleColor.White;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Connecction failed.");
                    Console.ForegroundColor = ConsoleColor.White;
                    Environment.Exit(1);
                }

                try
                {
                    string query = "INSERT INTO public.railway_route(name, length)" +
                                    "VALUES('" + name + "', " + length + "); ";

                    using (NpgsqlCommand command = new NpgsqlCommand(query, con))
                    {
                        command.ExecuteNonQuery();
                    }
                }
                catch
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Something went wrong.");
                    Console.ForegroundColor = ConsoleColor.White;
                    isOk = false;
                }
                return isOk;
            }
        }

        public bool UpdateRoute(int id, string name, int length)
        {
            using (NpgsqlConnection con = GetConnection())
            {
                bool isOk = true;

                con.Open(); //opens the connection
                if (con.State == ConnectionState.Open)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Conneccted to a DB.");
                    Console.ForegroundColor = ConsoleColor.White;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Connecction failed.");
                    Console.ForegroundColor = ConsoleColor.White;
                    Environment.Exit(1);
                }

                try
                {
                    string query = "UPDATE public.railway_route SET name='"+name+"', length="+length + " WHERE route_id ="+id;

                    using (NpgsqlCommand command = new NpgsqlCommand(query, con))
                    {
                        command.ExecuteNonQuery();
                    }
                }
                catch
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Something went wrong.");
                    Console.ForegroundColor = ConsoleColor.White;
                    isOk = false;
                }
                return isOk;
            }
        }

        public bool InputStation(string name, int trackNumber, int capacity)
        {
            using (NpgsqlConnection con = GetConnection())
            {
                bool isOk = true;

                con.Open(); //opens the connection
                if (con.State == ConnectionState.Open)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Conneccted to a DB.");
                    Console.ForegroundColor = ConsoleColor.White;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Connecction failed.");
                    Console.ForegroundColor = ConsoleColor.White;
                    Environment.Exit(1);
                }

                try
                {
                    string query = "INSERT INTO public.railway_station(name, track_number, capacity)" +
                                    "VALUES('" + name + "', " + trackNumber +", "+ capacity+ "); ";

                    using (NpgsqlCommand command = new NpgsqlCommand(query, con))
                    {
                        command.ExecuteNonQuery();
                    }
                }
                    catch
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Something went wrong.");
                    Console.ForegroundColor = ConsoleColor.White;
                    isOk = false;
                }
                return isOk;
            }
        }

        public bool UpdateStation(int id, string name, int trackNumber, int capacity)
        {
            using (NpgsqlConnection con = GetConnection())
            {
                bool isOk = true;

                con.Open(); //opens the connection
                if (con.State == ConnectionState.Open)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Conneccted to a DB.");
                    Console.ForegroundColor = ConsoleColor.White;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Connecction failed.");
                    Console.ForegroundColor = ConsoleColor.White;
                    Environment.Exit(1);
                }

                try
                {
                    string query = "UPDATE public.railway_station SET name='"+ name + "', capacity="+capacity +", track_number="+trackNumber+" WHERE station_id="+id;

                    using (NpgsqlCommand command = new NpgsqlCommand(query, con))
                    {
                        command.ExecuteNonQuery();
                    }
                }
                catch
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Something went wrong.");
                    Console.ForegroundColor = ConsoleColor.White;
                    isOk = false;
                }
                return isOk;
            }
        }

        public bool InputProfession(string name, string description)
        {
            using (NpgsqlConnection con = GetConnection())
            {
                bool isOk = true;

                con.Open(); //opens the connection
                if (con.State == ConnectionState.Open)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Conneccted to a DB.");
                    Console.ForegroundColor = ConsoleColor.White;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Connecction failed.");
                    Console.ForegroundColor = ConsoleColor.White;
                    Environment.Exit(1);
                }

                try
                {
                        string query = "INSERT INTO public.profession(name, description)" +
                                        "VALUES('" + name + "', '" + description + "'); ";

                        using (NpgsqlCommand command = new NpgsqlCommand(query, con))
                        {
                            command.ExecuteNonQuery();
                        }
                }
                    catch
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Something went wrong.");
                    Console.ForegroundColor = ConsoleColor.White;
                    isOk = false;
                }
                return isOk;
            }
        }

        public bool UpdateProfession(int id, string name, string description)
        {
            using (NpgsqlConnection con = GetConnection())
            {
                bool isOk = true;

                con.Open(); //opens the connection
                if (con.State == ConnectionState.Open)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Conneccted to a DB.");
                    Console.ForegroundColor = ConsoleColor.White;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Connecction failed.");
                    Console.ForegroundColor = ConsoleColor.White;
                    Environment.Exit(1);
                }

                try
                {
                    string query = "UPDATE public.profession SET name='"+name+ "', description='" + description + "' WHERE profession_id = "+id;

                    using (NpgsqlCommand command = new NpgsqlCommand(query, con))
                    {
                        command.ExecuteNonQuery();
                    }
                }
                catch
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Something went wrong.");
                    Console.ForegroundColor = ConsoleColor.White;
                    isOk = false;
                }
                return isOk;
            }
        }

        public bool InputEmployee(string name, string surname, int stationId, int professionId)
        {
            using (NpgsqlConnection con = GetConnection())
            {
                bool isOk = true;

                con.Open(); //opens the connection
                if (con.State == ConnectionState.Open)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Conneccted to a DB.");
                    Console.ForegroundColor = ConsoleColor.White;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Connecction failed.");
                    Console.ForegroundColor = ConsoleColor.White;
                    Environment.Exit(1);
                }

                try
                {
                    string query = "INSERT INTO public.employee (name, surname, work_station_id, employee_profession_id) "+
                                    "VALUES('"+name+ "', '" + surname+ "', " + stationId + ", "+professionId+"); ";

                    using (NpgsqlCommand command = new NpgsqlCommand(query, con))
                    {
                        command.ExecuteNonQuery();
                    }
                }
                catch
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Wrong data (probably can`t find station or profession id).");
                    Console.ForegroundColor = ConsoleColor.White;
                    isOk = false;
                }
                return isOk;
            }
        }

        public bool UpdateEmployee(int id, string name, string surname, int stationId, int professionId)
        {
            using (NpgsqlConnection con = GetConnection())
            {
                bool isOk = true;

                con.Open(); //opens the connection
                if (con.State == ConnectionState.Open)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Conneccted to a DB.");
                    Console.ForegroundColor = ConsoleColor.White;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Connecction failed.");
                    Console.ForegroundColor = ConsoleColor.White;
                    Environment.Exit(1);
                }

                try
                {
                    string query = "UPDATE public.employee SET name='" + name + "', surname='" + surname +
                                    "', work_station_id=" + stationId + ", employee_profession_id=" + professionId + " WHERE employee_id = " + id;

                    using (NpgsqlCommand command = new NpgsqlCommand(query, con))
                    {
                        command.ExecuteNonQuery();
                    }
                }
                catch
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Wrong data (probably can`t find station or profession id).");
                    Console.ForegroundColor = ConsoleColor.White;
                    isOk = false;
                }
                return isOk;
            }
        }

        public List<Route> GetAllRoutesWithStations()
        {
            using (NpgsqlConnection con = GetConnection())
            {
                List<Route> tempList = new List<Route>();

                con.Open(); //opens the connection
                if (con.State == ConnectionState.Open)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Conneccted to a DB.");
                    Console.ForegroundColor = ConsoleColor.White;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Connecction failed.");
                    Console.ForegroundColor = ConsoleColor.White;
                    Environment.Exit(1);
                }

                string query = "SELECT public.railway_route.route_id, public.railway_route.name, length, public.railway_station.station_id, public.railway_station.name, track_number, capacity " +
                                    "FROM public.r1 " +
                                        "INNER JOIN public.railway_route ON public.r1.route_id = public.railway_route.route_id " +
                                        "INNER JOIN public.railway_station ON public.r1.station_id = public.railway_station.station_id";

                Analyze(query);

                using (NpgsqlCommand command = new NpgsqlCommand(query, con))
                {
                    NpgsqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        // Проверяем, знакомый ли маршрут
                        bool isNew = true;
                        foreach(Route soloRoute in tempList)
                        {
                            if(soloRoute.id == int.Parse(reader[0].ToString()))
                            {
                                isNew = false;

                                Station tempStation;
                                tempStation.id = int.Parse(reader[3].ToString());
                                tempStation.name = reader[4].ToString();
                                tempStation.trackNumber = int.Parse(reader[5].ToString());
                                tempStation.capacity = int.Parse(reader[6].ToString());
                                soloRoute.stations.Add(tempStation);
                            }
                        }

                        if (isNew)
                        {
                            Route tempRoute;
                            tempRoute.id = int.Parse(reader[0].ToString());
                            tempRoute.name = reader[1].ToString();
                            tempRoute.length = int.Parse(reader[2].ToString());
                            tempRoute.stations = new List<Station>();

                            Station tempStation;
                            tempStation.id = int.Parse(reader[3].ToString());
                            tempStation.name = reader[4].ToString();
                            tempStation.trackNumber = int.Parse(reader[5].ToString());
                            tempStation.capacity = int.Parse(reader[6].ToString());

                            tempRoute.stations.Add(tempStation);
                            tempList.Add(tempRoute);
                        }
                    }
                }

                con.Close();

                return tempList;
            }
        }

        public List<Employee> GetAllEmployeesWithStations()
        {
            using (NpgsqlConnection con = GetConnection())
            {
                List<Employee> tempList = new List<Employee>();

                con.Open(); //opens the connection
                if (con.State == ConnectionState.Open)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Conneccted to a DB.");
                    Console.ForegroundColor = ConsoleColor.White;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Connecction failed.");
                    Console.ForegroundColor = ConsoleColor.White;
                    Environment.Exit(1);
                }

                string query = "SELECT employee_id, public.employee.name, surname, station_id, public.railway_station.name, profession_id, public.profession.name " +
                                    "FROM public.employee " +
                                        "INNER JOIN public.railway_station ON public.employee.work_station_id = public.railway_station.station_id " +
                                        "INNER JOIN public.profession ON public.employee.employee_profession_id = public.profession.profession_id";

                Analyze(query);

                using (NpgsqlCommand command = new NpgsqlCommand(query, con))
                {
                    NpgsqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        Employee tempSolo;

                        tempSolo.id = int.Parse(reader[0].ToString());
                        tempSolo.name = reader[1].ToString();
                        tempSolo.surname = reader[2].ToString();

                        tempSolo.stationId = int.Parse(reader[3].ToString());
                        tempSolo.stationName = reader[4].ToString();

                        tempSolo.professionId = int.Parse(reader[5].ToString());                       
                        tempSolo.professionName = reader[6].ToString();

                        tempList.Add(tempSolo);
                    }
                }

                con.Close();
                return tempList;
            }
        }

        public List<Profession> GetAllProfessions()
        {
            using (NpgsqlConnection con = GetConnection())
            {
                List<Profession> tempList = new List<Profession>();

                con.Open(); //opens the connection
                if (con.State == ConnectionState.Open)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Conneccted to a DB.");
                    Console.ForegroundColor = ConsoleColor.White;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Connecction failed.");
                    Console.ForegroundColor = ConsoleColor.White;
                    Environment.Exit(1);
                }

                string query1 = "SELECT profession_id, name, description FROM public.profession";

                Analyze(query1);

                using (NpgsqlCommand command1 = new NpgsqlCommand(query1, con))
                {
                    NpgsqlDataReader reader1 = command1.ExecuteReader();
                    
                    while (reader1.Read())
                    {
                        Profession tempSolo1;
                        
                        tempSolo1.id = int.Parse(reader1[0].ToString());
                        tempSolo1.name = reader1[1].ToString();
                        tempSolo1.description = reader1[2].ToString();

                        tempList.Add(tempSolo1);
                    }
                }

                con.Close();
                return tempList;
            }
        }

        public void GetAllStations()
        {
            using (NpgsqlConnection con = GetConnection())
            {
                con.Open(); //opens the connection
                if (con.State == ConnectionState.Open)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Conneccted to a DB.");
                    Console.ForegroundColor = ConsoleColor.White;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Connecction failed.");
                    Console.ForegroundColor = ConsoleColor.White;
                    Environment.Exit(1);
                }

                string query = "SELECT * FROM public.railway_station ORDER BY station_id ASC ";

                Analyze(query);

                using (NpgsqlCommand command = new NpgsqlCommand(query, con))
                {
                    NpgsqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        Console.ForegroundColor = ConsoleColor.Blue;
                        Console.WriteLine(reader[1].ToString() + " with " + reader[2].ToString() + " tracks.");
                        Console.ForegroundColor = ConsoleColor.White;
                    }
                }

                con.Close();
            }
        }

        public bool TestConnection()
        {
            using (NpgsqlConnection con = GetConnection())
            {
                con.Open();
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                    return true;                   
                }
                else
                {
                    con.Close();
                    return true;
                }              
            }
        }

        public bool Connect(int routeId, int stationId)
        {
            using (NpgsqlConnection con = GetConnection())
            {
                bool isOk = true;

                con.Open(); //opens the connection
                if (con.State == ConnectionState.Open)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Conneccted to a DB.");
                    Console.ForegroundColor = ConsoleColor.White;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Connecction failed.");
                    Console.ForegroundColor = ConsoleColor.White;
                    Environment.Exit(1);
                }

                try
                {
                    string query = "INSERT INTO public.r1(route_id, station_id)" +
                                    "VALUES('" + routeId + "', " + stationId + "); ";

                    using (NpgsqlCommand command = new NpgsqlCommand(query, con))
                    {
                        command.ExecuteNonQuery();
                    }
                }
                catch
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Wrong IDs.");
                    Console.ForegroundColor = ConsoleColor.White;
                    isOk = false;
                }
                return isOk;
            }
        }

        public bool Disconnect(int routeId, int stationId)
        {
            using (NpgsqlConnection con = GetConnection())
            {
                bool isOk = true;

                con.Open(); //opens the connection
                if (con.State == ConnectionState.Open)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Conneccted to a DB.");
                    Console.ForegroundColor = ConsoleColor.White;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Connecction failed.");
                    Console.ForegroundColor = ConsoleColor.White;
                    Environment.Exit(1);
                }

                try
                {
                    string query = "DELETE FROM public.r1 WHERE route_id="+routeId+" AND station_id="+stationId;

                    using (NpgsqlCommand command = new NpgsqlCommand(query, con))
                    {
                        command.ExecuteNonQuery();
                    }
                }
                catch
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Wrong IDs.");
                    Console.ForegroundColor = ConsoleColor.White;
                    isOk = false;
                }
                return isOk;
            }
        }

        private NpgsqlConnection GetConnection()
        {
            return new NpgsqlConnection(wholeParam);
        }

        public bool Recruit(int stationId, int professionId, int number)
        {
            using (NpgsqlConnection con = GetConnection())
            {
                bool isOk = true;

                con.Open(); //opens the connection
                if (con.State == ConnectionState.Open)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Conneccted to a DB.");
                    Console.ForegroundColor = ConsoleColor.White;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Connecction failed.");
                    Console.ForegroundColor = ConsoleColor.White;
                    Environment.Exit(1);
                }

                try
                {
                    for (int i = 0; i < number; i++)
                    {
                        string query = "INSERT INTO public.employee (name, surname, work_station_id, employee_profession_id) " +
                                    "VALUES((select chr(trunc(65+random()*25)::int)" +
                                        "|| chr(trunc(97 + random() * 25)::int)" +
                                        "|| chr(trunc(97 + random() * 25)::int)" +
                                        "|| chr(trunc(97 + random() * 25)::int)" +
                                        "|| chr(trunc(97 + random() * 25)::int)" +
                                        "|| chr(trunc(97 + random() * 25)::int)" +
                                        "from generate_series(1, 1))"+

                                        ", "+

                                        "(select chr(trunc(65 + random() * 25)::int)" +
                                        "|| chr(trunc(97 + random() * 25)::int)" +
                                        "|| chr(trunc(97 + random() * 25)::int)" +
                                        "|| chr(trunc(97 + random() * 25)::int)" +
                                        "|| chr(trunc(97 + random() * 25)::int)" +
                                        "from generate_series(1, 1)), "

                                    + stationId + ", " + professionId + "); ";

                        using (NpgsqlCommand command = new NpgsqlCommand(query, con))
                        {
                            command.ExecuteNonQuery();
                        }
                    }
                }
                catch
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Something went wrong.");
                    Console.ForegroundColor = ConsoleColor.White;
                    isOk = false;
                }
                return isOk;
            }
        }

        public bool RemoveEmployee(string name, string surname)
        {
            using (NpgsqlConnection con = GetConnection())
            {
                bool isOk = true;

                con.Open(); //opens the connection
                if (con.State == ConnectionState.Open)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Conneccted to a DB.");
                    Console.ForegroundColor = ConsoleColor.White;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Connecction failed.");
                    Console.ForegroundColor = ConsoleColor.White;
                    Environment.Exit(1);
                }

                try
                {
                    string query = "DELETE FROM public.employee WHERE name='"+name+"' AND surname='"+surname + "'";

                    using (NpgsqlCommand command = new NpgsqlCommand(query, con))
                    {
                        command.ExecuteNonQuery();
                    }
                }
                catch
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Something went wrong.");
                    Console.ForegroundColor = ConsoleColor.White;
                    isOk = false;
                }

                return isOk;
            }
        }

        public bool RemoveEmployee(int id)
        {
            using (NpgsqlConnection con = GetConnection())
            {
                bool isOk = true;

                con.Open(); //opens the connection
                if (con.State == ConnectionState.Open)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Conneccted to a DB.");
                    Console.ForegroundColor = ConsoleColor.White;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Connecction failed.");
                    Console.ForegroundColor = ConsoleColor.White;
                    Environment.Exit(1);
                }

                try
                {
                    string query = "DELETE FROM public.employee WHERE employee_id=" +id;

                    using (NpgsqlCommand command = new NpgsqlCommand(query, con))
                    {
                        command.ExecuteNonQuery();
                    }
                }
                catch
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Something went wrong.");
                    Console.ForegroundColor = ConsoleColor.White;
                    isOk = false;
                }

                return isOk;
            }
        }


        public bool RemoveProfession(string name)
        {
            using (NpgsqlConnection con = GetConnection())
            {
                bool isOk = true;

                con.Open(); //opens the connection
                if (con.State == ConnectionState.Open)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Conneccted to a DB.");
                    Console.ForegroundColor = ConsoleColor.White;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Connecction failed.");
                    Console.ForegroundColor = ConsoleColor.White;
                    Environment.Exit(1);
                }

                try
                {
                    string query = "DELETE FROM public.profession WHERE name='" + name+"'";

                    using (NpgsqlCommand command = new NpgsqlCommand(query, con))
                    {
                        command.ExecuteNonQuery();
                    }
                }
                catch
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Something went wrong.");
                    Console.ForegroundColor = ConsoleColor.White;
                    isOk = false;
                }

                return isOk;
            }
        }

        public bool RemoveProfession(int id)
        {
            using (NpgsqlConnection con = GetConnection())
            {
                bool isOk = true;

                con.Open(); //opens the connection
                if (con.State == ConnectionState.Open)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Conneccted to a DB.");
                    Console.ForegroundColor = ConsoleColor.White;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Connecction failed.");
                    Console.ForegroundColor = ConsoleColor.White;
                    Environment.Exit(1);
                }

                try
                {
                    string query = "DELETE FROM public.profession WHERE profession_id=" + id;

                    using (NpgsqlCommand command = new NpgsqlCommand(query, con))
                    {
                        command.ExecuteNonQuery();
                    }
                }
                catch
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Something went wrong.");
                    Console.ForegroundColor = ConsoleColor.White;
                    isOk = false;
                }

                return isOk;
            }
        }


        public bool RemoveStation(string name)
        {
            using (NpgsqlConnection con = GetConnection())
            {
                bool isOk = true;

                con.Open(); //opens the connection
                if (con.State == ConnectionState.Open)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Conneccted to a DB.");
                    Console.ForegroundColor = ConsoleColor.White;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Connecction failed.");
                    Console.ForegroundColor = ConsoleColor.White;
                    Environment.Exit(1);
                }

                try
                {
                    string query = "DELETE FROM public.railway_station WHERE name='" + name+"'";

                    using (NpgsqlCommand command = new NpgsqlCommand(query, con))
                    {
                        command.ExecuteNonQuery();
                    }
                }
                catch
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Something went wrong.");
                    Console.ForegroundColor = ConsoleColor.White;
                    isOk = false;
                }

                return isOk;
            }
        }

        public bool RemoveStation(int id)
        {
            using (NpgsqlConnection con = GetConnection())
            {
                bool isOk = true;

                con.Open(); //opens the connection
                if (con.State == ConnectionState.Open)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Conneccted to a DB.");
                    Console.ForegroundColor = ConsoleColor.White;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Connecction failed.");
                    Console.ForegroundColor = ConsoleColor.White;
                    Environment.Exit(1);
                }

                try
                {
                    string query = "DELETE FROM public.railway_station WHERE station_id=" + id;

                    using (NpgsqlCommand command = new NpgsqlCommand(query, con))
                    {
                        command.ExecuteNonQuery();
                    }
                }
                catch
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Something went wrong.");
                    Console.ForegroundColor = ConsoleColor.White;
                    isOk = false;
                }

                return isOk;
            }
        }

        public bool RemoveRoute(string name)
        {
            using (NpgsqlConnection con = GetConnection())
            {
                bool isOk = true;

                con.Open(); //opens the connection
                if (con.State == ConnectionState.Open)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Conneccted to a DB.");
                    Console.ForegroundColor = ConsoleColor.White;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Connecction failed.");
                    Console.ForegroundColor = ConsoleColor.White;
                    Environment.Exit(1);
                }

                try
                {
                    string query = "DELETE FROM public.railway_route WHERE name='" + name + "'";

                    using (NpgsqlCommand command = new NpgsqlCommand(query, con))
                    {
                        command.ExecuteNonQuery();
                    }
                }
                catch
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Something went wrong.");
                    Console.ForegroundColor = ConsoleColor.White;
                    isOk = false;
                }

                return isOk;
            }
        }

        public bool RemoveRoute(int id)
        {
            using (NpgsqlConnection con = GetConnection())
            {
                bool isOk = true;

                con.Open(); //opens the connection
                if (con.State == ConnectionState.Open)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Conneccted to a DB.");
                    Console.ForegroundColor = ConsoleColor.White;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Connecction failed.");
                    Console.ForegroundColor = ConsoleColor.White;
                    Environment.Exit(1);
                }

                try
                {
                    string query = "DELETE FROM public.railway_route WHERE route_id=" + id;

                    using (NpgsqlCommand command = new NpgsqlCommand(query, con))
                    {
                        command.ExecuteNonQuery();
                    }
                }
                catch
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Something went wrong.");
                    Console.ForegroundColor = ConsoleColor.White;
                    isOk = false;
                }

                return isOk;
            }
        }
    }
}
