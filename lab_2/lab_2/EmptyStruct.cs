﻿using System;
using System.Collections.Generic;

namespace lab_2
{
    public struct Employee
    {
        public int id;
        public string name;
        public string surname;
            
        public int stationId;
        public string stationName;

        public int professionId;
        public string professionName;
    }

    public struct Profession
    {
        public int id;
        public string name;
        public string description;
    }


    public struct Station
    {
        public int id;
        public string name;
        public int trackNumber;
        public int capacity;
    }

    public struct Route
    {
        public int id;
        public string name;
        public int length;

        public List<Station> stations;
    }
}
